/*
 * @file NT_Control.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Controller function declarations.
 */

#ifndef NT_Control_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define NT_Control_h

enum Type_t
{
	UNDEFINED = 1,
	DIGITAL = 2,
	ANALOG = 3
};

class Control_t
{
public:
	bool Enable = FALSE;
	int Current_Op = 0;
	void Configure(int Minimum_Op, int Maximum_Op, String Name);
	void Init();
	void Deinit();
	String Print();

private:
	String _Name;
	int _Minimum_Op = 0;
	int _Maximum_Op = 0;
};

#endif