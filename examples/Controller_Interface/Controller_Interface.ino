/*
   @file Controller.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Simple example of controller utilisation.
*/

#include <NT_Controller.h>

Controller_t Controller1;
long Last_Execution;

void setup()
{
  Controller1.Configure();
  Controller1.Attach();
  Controller1.Init();
  Controller1.Attach_Serial(&Serial1, 19200);
}

void loop()
{
  Controller1.Get();
  Controller1.Controller_Send(10);
  Controller1.Controller_Receive();
  Controller1.Set();

  // Print all data
  Controller1.Print_Controller_Values();
  Controller1.Print_Rx_Bytes();
  Controller1.Print_Tx_Bytes();
}
