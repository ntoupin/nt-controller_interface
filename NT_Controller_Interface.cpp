/*
 * @file NT_Controller.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Led function definitions.
 */

#include "Arduino.h"
#include "NT_Controller_Interface.h"

Controller_t::Controller_Interface_t()
{
}

void Controller_t::Attach_Serial(HardwareSerial* Serial_Port, int Baud_Rate)
{
	_Serial_Port = Serial_Port;
	_Baud_Rate = Baud_Rate;
}

//(int Minimum_Sp, int Maximum_Sp, int Minimum_Op, int Maximum_Op, int Direction, Type_t Type, String Name)
void Controller_t::Configure()
{
	Btn_Up.Configure(0, 1, "Up Button");
	Btn_Left.Configure(0, 1, "Left Button");
	Btn_Down.Configure(0, 1, "Down Button");
	Btn_Right.Configure(0, 1, "Right Button");
	Btn_1.Configure(0, 1, "Button 1");
	Btn_2.Configure(0, 1, "Button 2");
	Btn_3.Configure(0, 1, "Button 3");
	Btn_4.Configure(0, 1, "Button 4");
	Btn_Left_Z1.Configure(0, 1, "Z1 Button Left");
	Btn_Left_Z2.Configure(0, 1, "Z2 Button Left");
	Btn_Right_Z1.Configure(0, 1, "Z1 Button Right");
	Btn_Right_Z2.Configure(0, 1, "Z1 Button Right");
	Btn_Select.Configure(0, 1, "Select Button");
	Btn_Start.Configure(0, 1, "Start Button");
	Joy_Btn_Left.Configure(0, 1, "Joystick Button Left");
	Joy_Btn_Right.Configure(0, 1, "Joystick Button Right");
	Joy_Ana_Left_X.Configure(0, 1023, "Joystick Left X Axis");
	Joy_Ana_Left_Y.Configure(0, 1023, "Joystick Left Y Axis");
	Joy_Ana_Right_X.Configure(0, 1023, "Joystick Right X Axis");
	Joy_Ana_Right_Y.Configure(0, 1023, "Joystick Right Y Axis");
	Vib_Motor.Configure(0, 1, "Vibration Motor");
	Com_Led.Configure(0, 1, "Communication Led");
}

void Controller_t::Init()
{
	_Serial_Port->begin(_Baud_Rate);

	Btn_Up.Init();
	Btn_Left.Init();
	Btn_Down.Init();
	Btn_Right.Init();
	Btn_1.Init();
	Btn_2.Init();
	Btn_3.Init();
	Btn_4.Init();
	Btn_Left_Z1.Init();
	Btn_Left_Z2.Init();
	Btn_Right_Z1.Init();
	Btn_Right_Z2.Init();
	Btn_Select.Init();
	Btn_Start.Init();
	Joy_Btn_Left.Init();
	Joy_Btn_Right.Init();
	Joy_Ana_Left_X.Init();
	Joy_Ana_Left_Y.Init();
	Joy_Ana_Right_X.Init();
	Joy_Ana_Right_Y.Init();
	Vib_Motor.Init();
	Com_Led.Init();

	Enable = TRUE;
}

void Controller_t::Send(float Frequency)
{
	if (Enable == TRUE)
	{
		if ((millis() - _Last_Transfer) > (1000 / Frequency))
		{
			// Fill buffer
			_Tx_Buffer[0] = _Command_Byte;
			_Tx_Buffer[1] = 0x00;
			_Tx_Buffer[1] |= (byte)((Btn_Up.Current_Op & 0x01) << 0);
			_Tx_Buffer[1] |= (byte)((Btn_Left.Current_Op & 0x01) << 1);
			_Tx_Buffer[1] |= (byte)((Btn_Down.Current_Op & 0x01) << 2);
			_Tx_Buffer[1] |= (byte)((Btn_Right.Current_Op & 0x01) << 3);
			_Tx_Buffer[1] |= (byte)((Btn_1.Current_Op & 0x01) << 4);
			_Tx_Buffer[1] |= (byte)((Btn_2.Current_Op & 0x01) << 5);
			_Tx_Buffer[1] |= (byte)((Btn_3.Current_Op & 0x01) << 6);
			_Tx_Buffer[1] |= (byte)((Btn_4.Current_Op & 0x01) << 7);
			_Tx_Buffer[2] = 0x00;
			_Tx_Buffer[2] |= (byte)((Btn_Left_Z1.Current_Op & 0x01) << 0);
			_Tx_Buffer[2] |= (byte)((Btn_Left_Z2.Current_Op & 0x01) << 1);
			_Tx_Buffer[2] |= (byte)((Btn_Right_Z1.Current_Op & 0x01) << 2);
			_Tx_Buffer[2] |= (byte)((Btn_Right_Z2.Current_Op & 0x01) << 3);
			_Tx_Buffer[2] |= (byte)((Btn_Select.Current_Op & 0x01) << 4);
			_Tx_Buffer[2] |= (byte)((Btn_Start.Current_Op & 0x01) << 5);

			if (Joy_Btn_Left.Current_Op > 128)
			{
				_Tx_Buffer[2] |= (byte)(0x01 << 6);
			}
			else
			{
				_Tx_Buffer[2] |= (byte)(0x00 << 6);
			}

			if (Joy_Btn_Right.Current_Op > 128)
			{
				_Tx_Buffer[2] |= (byte)(0x01 << 7);
			}
			else
			{
				_Tx_Buffer[2] |= (byte)(0x00 << 7);
			}

			_Tx_Buffer[3] = (byte)Joy_Ana_Left_X.Current_Op;
			_Tx_Buffer[4] = (byte)Joy_Ana_Left_Y.Current_Op;
			_Tx_Buffer[5] = (byte)Joy_Ana_Right_X.Current_Op;
			_Tx_Buffer[6] = (byte)Joy_Ana_Right_Y.Current_Op;

			// Compute checksum
			_Tx_Buffer[BYTE_QTY_SEND - 1] = 0;
			for (int i = 0; i < (BYTE_QTY_SEND - 1); ++i)
			{
				_Tx_Buffer[BYTE_QTY_SEND - 1] ^= _Tx_Buffer[i];
			}

			// Send sync byte for alive flag
			_Serial_Port->write(_Sync_Byte);

			// Send to serial port
			for (int i = 0; i < BYTE_QTY_SEND; ++i)
			{
				_Serial_Port->write(_Tx_Buffer[i]);
			}
			_Last_Transfer = millis();

			Print_Tx_Bytes();
		}
	}
}

void Controller_t::Receive()
{
	if (Enable == TRUE)
	{
		// Check if bytes are available on Serial1 bus
		if (_Serial_Port->available() > 0)
		{
			// Wait for sync byte
			while (_Serial_Port->read() != _Sync_Byte);

			// Wait to have BYTE_QTY bytes into buffer
			while (_Serial_Port->available() < BYTE_QTY);

			// Reset alive counter to make sure controller is still there
			_Alive_Counter = 0;

			// Read next byte supposed to be the command
			if (_Serial_Port->read() == _Command_Byte)
			{
				_Rx_Buffer[0] = _Command_Byte;

				// Receive data
				for (int i = 1; i < BYTE_QTY; ++i)
				{
					_Rx_Buffer[i] = _Serial_Port->read();
				}

				// Compute checksum
				byte Checksum = 0;
				for (int i = 0; i < (BYTE_QTY - 1); ++i)
				{
					Checksum ^= _Rx_Buffer[i];
				}

				// Check if both checksum are the same
				if (Checksum == _Rx_Buffer[(BYTE_QTY - 1)])
				{
					Btn_Up.Current_Op = (int)((_Rx_Buffer[1] & 0x01) >> 0);
					Btn_Left.Current_Op = (int)((_Rx_Buffer[1] & 0x02) >> 1);
					Btn_Down.Current_Op = (int)((_Rx_Buffer[1] & 0x04) >> 2);
					Btn_Right.Current_Op = (int)((_Rx_Buffer[1] & 0x08) >> 3);
					Btn_1.Current_Op = (int)((_Rx_Buffer[1] & 0x10) >> 4);
					Btn_2.Current_Op = (int)((_Rx_Buffer[1] & 0x20) >> 5);
					Btn_3.Current_Op = (int)((_Rx_Buffer[1] & 0x40) >> 6);
					Btn_4.Current_Op = (int)((_Rx_Buffer[1] & 0x80) >> 7);
					Btn_Left_Z1.Current_Op = (int)((_Rx_Buffer[2] & 0x01) >> 0);
					Btn_Left_Z2.Current_Op = (int)((_Rx_Buffer[2] & 0x02) >> 1);
					Btn_Right_Z1.Current_Op = (int)((_Rx_Buffer[2] & 0x04) >> 2);
					Btn_Right_Z2.Current_Op = (int)((_Rx_Buffer[2] & 0x08) >> 3);
					Btn_Select.Current_Op = (int)((_Rx_Buffer[2] & 0x10) >> 4);
					Btn_Start.Current_Op = (int)((_Rx_Buffer[2] & 0x20) >> 5);
					Joy_Btn_Left.Current_Op = (int)((_Rx_Buffer[2] & 0x40) >> 6);
					Joy_Btn_Right.Current_Op = (int)((_Rx_Buffer[2] & 0x80) >> 7);
					Joy_Ana_Left_X.Current_Op = (int)_Rx_Buffer[3];
					Joy_Ana_Left_Y.Current_Op = (int)_Rx_Buffer[4];
					Joy_Ana_Right_X.Current_Op = (int)_Rx_Buffer[5];
					Joy_Ana_Right_Y.Current_Op = (int)_Rx_Buffer[6];
				}
			}

			Print_Rx_Bytes();
		}
	}			
}

void Controller_t::Deinit()
{
	Btn_Up.Deinit();
	Btn_Left.Deinit();
	Btn_Down.Deinit();
	Btn_Right.Deinit();
	Btn_1.Deinit();
	Btn_2.Deinit();
	Btn_3.Deinit();
	Btn_4.Deinit();
	Btn_Left_Z1.Deinit();
	Btn_Left_Z2.Deinit();
	Btn_Right_Z1.Deinit();
	Btn_Right_Z2.Deinit();
	Btn_Select.Deinit();
	Btn_Start.Deinit();
	Joy_Btn_Left.Deinit();
	Joy_Btn_Right.Deinit();
	Joy_Ana_Left_X.Deinit();
	Joy_Ana_Left_Y.Deinit();
	Joy_Ana_Right_X.Deinit();
	Joy_Ana_Right_Y.Deinit();
	Vib_Motor.Deinit();
	Com_Led.Deinit();

	Enable = FALSE;
}

void Controller_t::Init_Serial_Debug()
{
	Serial.begin(9600);
}

void Controller_t::Print_Controller_Values()
{
	Serial.print("CONTROLLER -> ");
	Serial.print(Btn_Up.Print());
	Serial.print(Btn_Left.Print());
	Serial.print(Btn_Down.Print());
	Serial.print(Btn_Right.Print());
	Serial.print(Btn_1.Print());
	Serial.print(Btn_2.Print());
	Serial.print(Btn_3.Print());
	Serial.print(Btn_4.Print());
	Serial.print(Btn_Left_Z1.Print());
	Serial.print(Btn_Left_Z2.Print());
	Serial.print(Btn_Right_Z1.Print());
	Serial.print(Btn_Right_Z2.Print());
	Serial.print(Btn_Select.Print());
	Serial.print(Btn_Start.Print());
	Serial.print(Joy_Btn_Left.Print());
	Serial.print(Joy_Btn_Right.Print());
	Serial.print(Joy_Ana_Left_X.Print());
	Serial.print(Joy_Ana_Left_Y.Print());
	Serial.print(Joy_Ana_Right_X.Print());
	Serial.print(Joy_Ana_Right_Y.Print());
	Serial.println("End");
}

void Controller_t::Print_Tx_Bytes()
{
	Serial.print("TX - ");
	Serial.print(_Sync_Byte, HEX);
	Serial.print(" - ");

	for (int i = 0; i < BYTE_QTY_SEND; ++i)
	{
		Serial.print(_Tx_Buffer[i], HEX);
		Serial.print(" - ");
	}

	Serial.print("TIME : ");
	Serial.print(_Last_Transfer);
	Serial.println(".");
}

void Controller_t::Print_Rx_Bytes()
{
	Serial.print("RX - ");
	Serial.print(_Sync_Byte, HEX);
	Serial.print(" - ");

	for (int i = 0; i < BYTE_QTY_SEND; ++i)
	{
		Serial.print(_Rx_Buffer[i], HEX);
		Serial.print(" - ");
	}

	Serial.print("TIME : ");
	Serial.print(micros());
	Serial.println(".");
}