/*
 * @file NT_Control.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Led function definitions.
 */

#include "Arduino.h"
#include "NT_Control.h"

void Control_t::Configure(int Minimum_Op, int Maximum_Op, String Name)
{
	_Minimum_Op = Minimum_Op;
	_Maximum_Op = Maximum_Op;
	_Name = Name;
}

void Control_t::Init()
{
	Enable = TRUE;
}

void Control_t::Deinit()
{
	Enable = FALSE;
}

String Control_t::Print()
{
	return String(String(Current_Op) + " - ");
}