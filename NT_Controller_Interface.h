/*
 * @file NT_Controller.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Controller function declarations.
 */

#ifndef NT_Controller_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define BYTE_QTY_SEND 8
#define BYTE_QTY_RECEIVE 3

#define NT_Controller_h

class Controller_Interface_t
{
public:
	Controller_Interface_t();
	bool Enable = FALSE;
	Control_t Btn_Up;
	Control_t Btn_Left;
	Control_t Btn_Down;
	Control_t Btn_Right;
	Control_t Btn_1;
	Control_t Btn_2;
	Control_t Btn_3;
	Control_t Btn_4;
	Control_t Btn_Left_Z1;
	Control_t Btn_Left_Z2;
	Control_t Btn_Right_Z1;
	Control_t Btn_Right_Z2;
	Control_t Btn_Select;
	Control_t Btn_Start;
	Control_t Joy_Btn_Left;
	Control_t Joy_Btn_Right;
	Control_t Joy_Ana_Left_X;
	Control_t Joy_Ana_Left_Y;
	Control_t Joy_Ana_Right_X;
	Control_t Joy_Ana_Right_Y;
	Control_t Vib_Motor;
	Control_t Com_Led;	
	void Attach_Serial(HardwareSerial* Serial_Port, int Baud_Rate);
	void Configure();
	void Init();
	void Send(float Frequency);
	void Receive();
	void Deinit();
	void Init_Serial_Debug();
	void Print_Controller_Values();
	void Print_Tx_Bytes();
	void Print_Rx_Bytes();

private:
	HardwareSerial* _Serial_Port;	
	int _Baud_Rate = 19200;
	bool _Alive_Flag = FALSE;
	long _Alive_Counter = 0;
	byte _Sync_Byte = 0xFE;
	byte _Command_Byte = 0xEF;
	int _Tx_Buffer[BYTE_QTY_SEND] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	int _Rx_Buffer[BYTE_QTY_SEND] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	int _Checksums[CHECKSUM_QTY] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	long _Last_Transfer = 0;
	int _Execution_Time = 0;
};

#endif